 <?php
include 'config.php';


 ?>

 <?php
/* 
 EDIT.PHP
 Allows user to edit specific entry in database
*/

 // creates the edit record form
 // since this form is used multiple times in this file, I have made it a function that is easily reusable
 function renderForm($id, $name, $slug, $description, $price, $featured_image)
 {
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
 <html>
 <head>
 <title>Edit Record</title>
 </head>
 <body>
 <?php 
 // if there are any errors, display them
 
 ?> 
 
 <form action="" method="post">
 <input type="hidden" name="id" value="<?php echo $id; ?>"/>
 <div>
 	    	<?php echo "<img src=uploads/".$featured_image;">"; ?>
 <p><strong>ID:</strong> <?php echo $id; ?></p>
 <strong> Name: *</strong> <input type="text" name="name" value="<?php echo $name; ?>"/><br/>
 <strong>Slug: *</strong> <input type="text" name="slug" value="<?php echo $slug; ?>"/><br/>
  <strong>Description: *</strong> <input ols="30" row="10" type="text" name="description" value="<?php echo $description; ?>"/><br/>
   <strong>Price: *</strong> <input type="text" name="price" value="<?php echo $price; ?>"/><br/>
   <input  name="featured_image" type="file"><br><?php echo $featured_image; ?>
  <?php
include 'config.php';

$category = $conn->query('SELECT * FROM category');

?>

       <div class="strip">
        	
	<?php

while($rc = $category->fetch()){


?>
<input type="checkbox" name="id" value="<?php echo $rc['id']; ?>"  /><?php echo $rc['name']; ?>
<?php
}?>

 <input type="submit" name="submit" value="Submit">
 </div>
 </form> 
 </body>
 </html> 
 <?php
 }



 // connect to the database
 include('config.php');
 
 // check if the form has been submitted. If it has, process the form and save it to the database
 if (isset($_POST['submit']))
 { 
 // confirm that the 'id' value is a valid integer before getting the form data
 if (is_numeric($_POST['id']))
 {
 // get form data, making sure it is valid
 $id = $_POST['id'];
 $name = $_POST['name'];
 $slug = $_POST['slug'];
 $description = $_POST['description'];
 $price = $_POST['price'];
  $featured_image = $_POST['featured_image'];
 
 // check that firstname/lastname fields are both filled in
 if ($name == '' || $slug == '')
 {
 // generate error message
 $error = 'ERROR: Please fill in all required fields!';
 
 //error, display form
renderForm($id, $name, $slug, $description, $price, $featured_image);
 }
 else
 {
 // save the data to the database
 $products = $conn->query("UPDATE products SET name='$name', slug='$slug', description='$description', price='$price' , featured_image='$featured_image'WHERE id='$id'");

 
 // once saved, redirect back to the view page
 header("Location: info.php"); 
 }
 }
 else
 {
 // if the 'id' isn't valid, display an error
 echo 'Error!';
 }
 }
 else
 // if the form hasn't been submitted, get the data from the db and display the form
 {
 
 // get the 'id' value from the URL (if it exists), making sure that it is valid (checing that it is numeric/larger than 0)
 if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)
 {
 // query db
 $id = $_GET['id'];
 $products = $conn->query( "SELECT * FROM products WHERE id=$id"); 

$rp = $products->fetch();
 
 // check that the 'id' matches up with a row in the databse
 if($rp)
 {
 
 // get data from db
 $name = $rp['name'];
 $slug = $rp['slug'];
  $description = $rp['description'];
  $price = $rp['price'];
 $featured_image = $rp['featured_image'];
 // show form
renderForm($id, $name, $slug, $description, $price, $featured_image);
 }
 else
 // if no match, display result
 {
 echo "No results!";
 }
 }
 else
 // if the 'id' in the URL isn't valid, or if there is no 'id' value, display an error
 {
 echo 'Error!';
 }
 }
?>