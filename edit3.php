 <?php
include 'config.php';


 ?>

 <?php
/* 
 EDIT.PHP
 Allows user to edit specific entry in database
*/

 // creates the edit record form
 // since this form is used multiple times in this file, I have made it a function that is easily reusable
 function renderForm($id, $name, $slug)
 {
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
 <html>
 <head>
 <title>Edit Category</title>
 </head>
 <body>
 <?php 
 // if there are any errors, display them
 
 ?> 
 
 <form action="" method="post">
 <input type="hidden" name="id" value="<?php echo $id; ?>"/>
 <div>
 <p><strong>ID:</strong> <?php echo $id; ?></p>
 <strong> Name: *</strong> <input type="text" name="name" value="<?php echo $name; ?>"/><br/>
 <strong>Slug: *</strong> <input type="text" name="slug" value="<?php echo $slug; ?>"/><br/>
  <input type="submit" name="submit" value="Submit">
 </div>
 </form> 
 </body>
 </html> 
 <?php
 }



 // connect to the database
 include('config.php');
 
 // check if the form has been submitted. If it has, process the form and save it to the database
 if (isset($_POST['submit']))
 { 
 // confirm that the 'id' value is a valid integer before getting the form data
 if (is_numeric($_POST['id']))
 {
 // get form data, making sure it is valid
 $id = $_POST['id'];
 $name = $_POST['name'];
 $slug = $_POST['slug'];

 // check that firstname/lastname fields are both filled in
 if ($name == '' || $slug == '')
 {
 // generate error message
 $error = 'ERROR: Please fill in all required fields!';
 
 //error, display form
renderForm($id, $name, $slug);
 }
 else
 {
 // save the data to the database
  $category = $conn->query("UPDATE category SET name='$name', slug='$slug' WHERE id='$id'"); 
 
 // once saved, redirect back to the view page
 header("Location: category.php"); 
 }
 }
 else
 {
 // if the 'id' isn't valid, display an error
 echo 'Error!';
 }
 }
 else
 // if the form hasn't been submitted, get the data from the db and display the form
 {
 
 // get the 'id' value from the URL (if it exists), making sure that it is valid (checing that it is numeric/larger than 0)
 if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)
 {
 // query db
 $id = $_GET['id'];
$category = $conn->query("SELECT * FROM category WHERE id=$id"); 

$rc = $category->fetch();
 
 // check that the 'id' matches up with a row in the databse
 if($rc)
 {
 
 // get data from db
 $name = $rc['name'];
 $slug = $rc['slug'];

renderForm($id, $name, $slug);
 }
 else
 // if no match, display result
 {
 echo "No results!";
 }
 }
 else
 // if the 'id' in the URL isn't valid, or if there is no 'id' value, display an error
 {
 echo 'Error!';
 }
 }
?>