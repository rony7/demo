$(document).ready(function(){
  $('input').iCheck({
	checkboxClass: 'icheckbox_square-aero',
	radioClass: 'iradio_square-aero',
	increaseArea: '20%' // optional
  });
  
$("span.closepopup").click(function(){  
	$(".cartpop").slideToggle(600);
});  


$('.cart-amount').click(function() {
	$('.cartpop').fadeToggle(500); 	
});	


$(window).scroll(function(){
	if ($(this).scrollTop() > 100) {
		$('.scrollup').fadeIn();
	} else {
		$('.scrollup').fadeOut();
	}
});

$('.scrollup').click(function(){
	$("html, body").animate({ scrollTop: 0 }, 600);
	return false;
});

$(window).scroll(function(){
	if ($(this).scrollTop() > 100) {
		$('.scrollup').fadeIn();
	} else {
		$('.scrollup').fadeOut();
	}
});

$('.scrollup, .totop').click(function(){
	$("html, body").animate({ scrollTop: 0 }, 600);
	return false;
});	

  
  
}); //close doc ready

//Features accordion
$(document).on('click', '.feature-open.closed', function(){
	$("dl.feature-list").slideUp(500);
	$(".arrow1").html("").html("<i class='fa fa-angle-down'>");
	$(this).addClass("open");
	$(this).removeClass("closed");
});

$(document).on('click', '.feature-open.open', function(){
	$("dl.feature-list").slideDown(500);
	$(".arrow1").html("").html("<i class='fa fa-angle-up'>");
	$(this).removeClass("open");
	$(this).addClass("closed");
});

//Brands accordion
$(document).on('click', '.brands-open.closed', function(){
	$("dl.brands-list").slideUp(500);
	$(".arrow2").html("").html("<i class='fa fa-angle-down'>");
	$(this).addClass("open");
	$(this).removeClass("closed");
});

$(document).on('click', '.brands-open.open', function(){
	$("dl.brands-list").slideDown(500);
	$(".arrow2").html("").html("<i class='fa fa-angle-up'>");
	$(this).removeClass("open");
	$(this).addClass("closed");
});

//Category accordion
$(document).on('click', '.category-open.closed', function(){
	$("dl.category-list").slideUp(500);
	$(".arrow3").html("").html("<i class='fa fa-angle-down'>");
	$(this).addClass("open");
	$(this).removeClass("closed");
});

$(document).on('click', '.category-open.open', function(){
	$("dl.category-list").slideDown(500);
	$(".arrow3").html("").html("<i class='fa fa-angle-up'>");
	$(this).removeClass("open");
	$(this).addClass("closed");
});
//filterby accordion
$(document).on('click', '.sidebarblock.filterby dl dt', function(){
	$(this).find("dl").slideToggle(500);
});

$(document).on('click', '.category-open.open', function(){
	$("dl.category-list").slideDown(500);
	$(".arrow3").html("").html("<i class='fa fa-angle-up'>");
	$(this).removeClass("open");
	$(this).addClass("closed");
});

//Single product description accordion
$(document).on('click', '.opendescription.closed', function(){
	$(".desc-box").slideDown(500);
	$(".arrow5").html("").html("<i class='fa fa-angle-down'>");
	$(this).addClass("open");
	$(this).removeClass("closed");	
});
$(document).on('click', '.opendescription.open', function(){
	$(".desc-box").slideUp(500);
	$(".arrow5").html("").html("<i class='fa fa-angle-up'>");
	$(this).addClass("closed");
	$(this).removeClass("open");
});

//Single product details accordion
$(document).on('click', '.opendetail.closed', function(){
	$(".info-box").slideDown(500);
	$(".arrow6").html("").html("<i class='fa fa-angle-down'>");
	$(this).addClass("open");
	$(this).removeClass("closed");	
});
$(document).on('click', '.opendetail.open', function(){
	$(".info-box").slideUp(500);
	$(".arrow6").html("").html("<i class='fa fa-angle-up'>");
	$(this).addClass("closed");
	$(this).removeClass("open");
});




	
	function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 300,
                header = document.querySelector("header");
                nav = document.querySelector(".navbar-default");		
		if ($(window).width() < 1200) {
		} else {		
				
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
                classie.add(nav,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                    classie.remove(nav,"smaller");
                }
            }
			
			}
			
        });
    }
    
	init();